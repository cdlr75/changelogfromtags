Welcome to changelogfromtags's documentation!
=============================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   README.md
   CHANGELOG.md
