FROM python:3.6
MAINTAINER Constantin De La Roche <cdlr75@gmail.com>

# check our environment
RUN python3 --version
RUN pip3 --version
RUN git --version

# Install the package
COPY . /tmp/app
RUN pip install /tmp/app
# Clean up
RUN rm -r /tmp/app

WORKDIR  /app

# Running Python Application
CMD ["changelogfromtags"]
