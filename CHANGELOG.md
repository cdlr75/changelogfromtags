Change Log
==========

0.2.1 (09/01/2020)
------------------
Make a beautifull change log

0.2.0 (09/01/2020)
------------------
Add argparse and -p --prefix option to add a prefix on each message line if prefix is not already present

0.1.1 (06/01/2020)
------------------
Deploy package changelogfromtags on pypi

0.1.0 (06/01/2020)
------------------
First release
You retrieve the content of `git tag 0.1.0 -n200`

